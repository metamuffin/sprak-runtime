import { readFile } from "fs/promises";
import { Program, Scope, Value } from "../lib/interpreter";
import { parse_program } from "../lib/parser";


class MeteorologyServer {
    ReadHumidity(): number {
        return Math.random() * 3
    }
}

function create_env(): Scope {
    const s = new Map<string, Value>()
    s.set("Say", (message: Value) => {
        console.log(`[ ${message} ]`);
    })
    s.set("Print", console.error)
    s.set("PrintS", s => process.stderr.write(s?.toString() ?? ""))
    s.set("ClearText", () => process.stderr.write("\x1b[1;1H\x1b[2J"))

    const met_server = new MeteorologyServer()
    s.set("Connect", (address) => {
        if (address == "MeteorologyServer") return met_server
        else throw new Error(`${address} does not exist`);
    })

    s.set("GetRoom", () => "Testing")
    s.set("FindPath", () => ["Testing1", "Testing2"])
    s.set("SaveData", (data: string) => console.log(data))

    s.set("GetPosition", (thing: string) => "Position of " + thing)
    s.set("GetThingsInRoom", (room: string) => ["ThingA", "ThingB"])
    s.set("GetAllRooms", () => ["Room1", "Room2", "Room3", "Room4", "Room5", "Room7", "Room8", "Room1", "Room2", "Room3", "Room4", "Room5", "Room7", "Room8", "Room1", "Room2", "Room3", "Room4", "Room5", "Room7", "Room8", "Room1", "Room2", "Room3", "Room4", "Room5", "Room7", "Room8"])
    s.set("ClearData", () => { })

    s.set("Count", (a: any[]) => a.length)

    s.set("_sq", "'")
    s.set("_dq", '"')

    return s
}


async function main() {
    const env = create_env()
    const code = (await readFile(process.argv[2])).toString()
    const ast = parse_program(code)
    // console.log(JSON.stringify(ast, null, 2));
    const prog = new Program(ast, env)
    // prog.processor_speed = 500
    // prog.verbosity = 10
    prog.run()
}

main()


