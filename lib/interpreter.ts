import { AST } from "./ast";

export type Scope = Map<string, Value>
export type Stack = Scope[]

abstract class Marker { }
class BreakMarker extends Marker { }
class ContinueMarker extends Marker { }
class ReturnMarker extends Marker { value: Value; constructor(value: Value) { super(); this.value = value } }
export type Value = number | boolean | string
    | Value[]
    | ((...args: Value[]) => Promise<Value> | Value)
    | (undefined | void | null)
    | Marker
    | { [key: string]: Value }

export class Program {
    processor_speed?: number
    code: AST
    env: Scope
    verbosity: number = 0

    constructor(code: AST, env: Scope) {
        this.code = code
        this.env = env
    }

    async delay() {
        if (this.processor_speed) await new Promise(r => setTimeout(r, 1000 / (this.processor_speed ?? 1000)))
    }
    async pre_step(kind: AST["kind"], depth: number, stack: Stack) {
        if (this.verbosity > 1) console.log(`${" ".repeat(depth)}> ${kind}`);
        await this.delay()
    }

    async run() {
        this.eval(0, [this.env, new Map()], this.code)
    }

    stack_get(stack: Stack, varname: string): Value {
        for (let i = stack.length - 1; i >= 0; i--) {
            const s = stack[i];
            if (s.get(varname) !== undefined) return s.get(varname)
        }
    }

    async eval(depth: number, stack: Stack, node: AST): Promise<Value> {
        await this.pre_step(node.kind, depth, stack)
        const eval_node = (stack: Stack, node: AST) => this.eval(depth + 1, stack, node)
        const eval_here = (node: AST) => this.eval(depth + 1, stack, node)
        const E: { [key in typeof node.kind]: (n: AST & { kind: key }) => Promise<Value> } = {
            compound: async (n) => {
                let r: Value
                for (const c of n.statements) {
                    r = await this.eval(depth + 1, stack, c)
                    if (r instanceof Marker) return r
                }
            },
            "literal-number": async (n) => n.value,
            "literal-string": async (n) => n.value,
            "ref": async (n) => {
                const v = this.stack_get(stack, n.name)
                if (v === undefined) {
                    console.error("STACK AT ERROR: ", stack);
                    throw new Error(`variable undefined: ${n.name}`);
                }
                return v
            },
            assign: async (n) => {
                let value = await eval_here(n.value)
                if (n.op) {
                    const op_fn = new Function("a", "b", `return (a ${n.op} b)`)
                    value = op_fn(this.stack_get(stack, n.name), value)
                }
                for (let i = stack.length - 1; i >= 0; i--) {
                    const s = stack[i];
                    if (s.get(n.name) !== undefined) { s.set(n.name, value); return }
                }
                throw new Error("variable not declared: " + n.name);
            },
            decl: async (n) => {
                const value = await eval_here(n.value)
                const top = stack[stack.length - 1]
                top.set(n.name, value)
            },
            empty: async () => { },
            "if": async (n) => {
                const cond = await eval_here(n.condition)
                if (cond) return await eval_here(n.case_true)
                else return await eval_here(n.case_false)
            },
            "function": async (n) => {
                const top = stack[stack.length - 1]
                top.set(n.name, async function (...args: Value[]) {
                    const fn_scope = new Map()
                    const fn_stack = [...stack, fn_scope]
                    for (let i = 0; i < n.arguments.length; i++)
                        fn_scope.set(n.arguments[i].name, args[i])
                    if (n.kind != "function") throw new Error("wtf. this is impossible");
                    let r = await eval_node(fn_stack, n.body)
                    if (r instanceof ReturnMarker) {
                        return r.value
                    } 
                })
            },
            call: async (n) => {
                const fn = await eval_here(n.fn)
                const args = []
                for (const a of n.arguments)
                    args.push(await eval_here(a))
                if (typeof fn == "function")
                    return await fn(...args)
                else {
                    console.error(fn);
                    throw new Error("tried to call value of a non-function type");
                }
            },
            "binary-operation": async (n) => {
                const fn = new Function("a", "b", `return (a ${n.op} b)`) // evil and dangerous
                const a = await eval_here(n.a)
                const b = await eval_here(n.b)
                return fn(a, b)
            },
            "unary-operation": async (n) => {
                const fn = new Function("a", `return (${n.op} a)`) // evil and dangerous
                return fn(await eval_here(n.a))
            },
            loop: async (n) => {
                const scope = new Map()
                const loop_stack = [...stack, scope]
                if (n.array) {
                    const array = await eval_here(n.array)
                    if (!(array instanceof Array)) throw new Error("loop over non-array type");
                    let r: Value
                    for (const e of array) {
                        if (n.el) scope.set(n.el, e)
                        r = await eval_node(loop_stack, n.body)
                        if (r instanceof ContinueMarker) continue
                        if (r instanceof BreakMarker) break
                        if (r instanceof Marker) return r
                    }
                } else {
                    let r: Value
                    while (1) {
                        r = await eval_node(loop_stack, n.body)
                        if (r instanceof ContinueMarker) continue
                        if (r instanceof BreakMarker) break
                        if (r instanceof Marker) return r
                    }
                }

            },
            member: async (n) => {
                const b = await eval_here(n.base)
                if (typeof b != "object" || b == null) throw new Error("cannot get members of non-object type");
                if (b instanceof Marker) throw new Error("nooooooooooooooo that cant happen");
                // if (!b.hasOwnProperty(n.name)) throw new Error("member does not exist: " + n.name);
                if (n.name.startsWith("__") || n.name == "prototype") throw new Error("member does not exist: " + n.name);
                if (b instanceof Array) throw new Error("cannot get members of array");
                return b[n.name]
            },
            return: async (n) => new ReturnMarker(await eval_here(n.value)),
            "literal-array": async (n) => {
                const a = []
                for (const e of n.elements) {
                    a.push(await eval_here(e))
                }
                return a
            },
            break: async (n) => new BreakMarker(),
            continue: async (n) => new ContinueMarker(),
            index: async (n) => {
                const a = await eval_here(n.array)
                const i = await eval_here(n.index)
                if (typeof i != "number") throw new Error("index is not a number");
                if (!(a instanceof Array)) throw new Error("array is not an array");
                return a[i]
            },

        }
        //@ts-ignore
        const r = await E[node.kind](node)
        // console.log(" ".repeat(depth + 1) + "-", r);
        return r
    }

}

