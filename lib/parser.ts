import { AST } from "./ast";
import { split1, split1br, splitbr } from "./helper";

export interface ProgramEnv {
    node_delay?: number
    env: Map<string, Function>
}

export function parse_program(i: string): AST {
    return parse_compound(i)
}

function parse_compound(i: string): AST {
    let lines = i.split("\n")
    let o: AST[] = []
    for (let i = 0; i < lines.length; i++) {
        const line = lines[i].trim();
        if (line.trim() == "") continue
        if (line.startsWith("#")) continue

        const read_body = () => {
            const start_line = i
            let br = 0
            while (1) {
                // console.log(br, lines[i]);
                if (lines[i].trim().startsWith("if ")
                    || lines[i].trim().startsWith("loop ")
                    || /^(var|string|number|array|void) ([a-z0-9_]+)\((.*)\)/i.exec(lines[i].trim())
                    || lines[i].trim() == "loop") br++
                // || lines[i].trim() == "else")
                if (lines[i].trim() == "end") br--
                if (br == 0) break
                i++
            }
            return parse_compound(lines.slice(start_line + 1, i).join("\n"))
        }

        if (line == "loop"
            || line.startsWith("loop ")
            || line.startsWith("if ")) {
            const sline = lines[i].trim()
            const body = read_body()
            if (line.startsWith("if ")) {
                const c = sline.substr("if ".length)
                o.push({
                    kind: "if",
                    condition: parse_other(c),
                    case_true: body,
                    case_false: { kind: "empty" }
                })
                continue
            }
            if (line == "loop") {
                o.push({
                    kind: "loop",
                    body,
                })
                continue
            }
            if (line.startsWith("loop ")) {
                const c = sline.substr("loop ".length)
                const [v, a] = split1br(c, " in ")
                if (a) {
                    o.push({
                        kind: "loop",
                        array: parse_other(a),
                        el: v,
                        body
                    })
                    continue
                }
                o.push({
                    kind: "loop",
                    array: parse_other(c),
                    body
                })
                continue
            }
            throw new Error("nuuuuuuuuuuu");
        }

        if (line.startsWith("return ")) {
            o.push({
                kind: "return",
                value: parse_other(line.substr("return ".length))
            })
            continue
        }
        if (line == "continue") {
            o.push({ kind: "continue" }); continue
        }
        if (line == "break") {
            o.push({ kind: "break" }); continue
        }

        const func_decl = /^(\w+) ([a-z0-9_]+)\((.*)\)/i.exec(line)
        if (func_decl) {
            const ret_type = func_decl[1]
            const fn_name = func_decl[2]
            const args = splitbr(func_decl[3], ",")
                .map(a => a.trim())
                .map(a => split1(a, " "))
                .map(([t, v]) => ({ type: t, name: v ?? "aaaaaaaaaaaaaa" }));
            const body = read_body()
            o.push({
                kind: "function",
                name: fn_name,
                arguments: args,
                body
            })
            continue
        }

        let flag = false
        for (const op of ["=", "+=", "-=", "*=", "/=", "%="]) {
            const [name_, value_] = split1br(line, op)
            const [name, value] = [name_.trim(), value_?.trim()]
            if (value) {
                if (op == "=") {
                    if (name.trim().split(" ").length > 1) {
                        const name_name = name.split(" ")[1]
                        const name_type = name.split(" ")[0]
                        if (/^[a-z0-9_\.]+$/i.test(name_name)) {
                            o.push({
                                kind: "decl",
                                name: name_name,
                                type: name_type,
                                value: parse_other(value)
                            })
                            flag = true
                            break
                        }
                    }
                }
                if (!/^[a-z0-9_\.]+$/i.test(name)) continue
                o.push({
                    kind: "assign",
                    op: op != "=" ? op.charAt(0) : undefined,
                    name: name.trim(),
                    value: parse_other(value)
                })
                flag = true
                break
            }
        }
        if (flag) continue
        o.push(parse_other(line))
    }
    return { kind: "compound", statements: o }
}

function parse_other(i: string): AST {
    i = i.trim()
    if (i.startsWith("(") && i.endsWith(")")) i = i.substring(1, i.length)

    const n = parseFloat(i)
    if (!Number.isNaN(n)) return {
        kind: "literal-number",
        value: n
    }
    if (i.startsWith("\"") && i.endsWith("\"") && !i.substring(1, i.length - 1).split("").includes("\"")) {
        return {
            kind: "literal-string",
            value: i.substring(1, i.length - 1)
        }
    }
    if (i.startsWith("'") && i.endsWith("'") && !i.substring(1, i.length - 1).split("").includes("'")) {
        return {
            kind: "literal-string",
            value: i.substring(1, i.length - 1)
        }
    }
    for (const o of ["==", "<", ">", "!=", "+", "-", "/", "*", "%"] as const) {
        const [a, b] = split1br(i, o)
        if (b) {
            return {
                kind: "binary-operation",
                op: o,
                a: parse_other(a),
                b: parse_other(b),
            }
        }
    }
    if (i.startsWith("!")) {
        return {
            kind: "unary-operation",
            op: "!",
            a: parse_other(i.substr(1))
        }
    }

    if (/^[a-z0-9_]+$/i.test(i)) {
        return {
            kind: "ref",
            name: i
        }
    }

    if (i.startsWith("[") && i.endsWith("]")) {
        const inner = i.substring(1, i.length - 1)
        const els = splitbr(inner, ",").map(parse_other)
        return {
            kind: "literal-array",
            elements: els
        }
    }

    const s_member = /^(.+)\.([a-z0-9_]+)$/i.exec(i)
    if (s_member) {
        const base_r = s_member[1]
        const base = parse_other(base_r)
        const member = s_member[2]
        return {
            kind: "member",
            base, name: member
        }
    }

    const s_index = /^(.+)\[(.+)\]$/i.exec(i)
    if (s_index) {
        const array_r = s_index[1]
        const index_r = s_index[2]
        const array = parse_other(array_r)
        const index = parse_other(index_r)
        return {
            kind: "index",
            array, index
        }
    }

    const s_call = /^([a-z0-9_\.]+)\(.*\)$/i.exec(i)
    if (s_call) {
        const fn_name = s_call[1]
        const args_r = i.substring(fn_name.length + 1, i.length - 1)
        const fn = parse_other(fn_name)
        const args = splitbr(args_r, ",").map(parse_other)
        return {
            kind: "call",
            arguments: args,
            fn
        }
    }

    throw new Error("unknown statement: " + i);
}
