
export type AST = never
    | AIf
    | ACompound
    | ALoop
    | ADecl
    | AAssign
    | AReturn
    | AFunction
    | AEmpty
    | ABinaryOperation
    | AUnaryOperation
    | AStringLiteral
    | ANumberLiteral
    | AReference
    | ACall
    | AMember
    | AArrayLiteral
    | AIndex
    | ABreak
    | AContinue

export interface AEmpty { kind: "empty" }
export interface ACompound {
    kind: "compound",
    statements: AST[]
}
export interface AIf {
    kind: "if",
    condition: AST
    case_true: AST
    case_false: AST
}
export interface ALoop {
    kind: "loop"
    el?: string
    array?: AST
    body: AST
}
export interface ABreak {
    kind: "break"
}
export interface AContinue {
    kind: "continue"
}
export interface AIndex {
    kind: "index"
    array: AST
    index: AST
}
export interface ADecl {
    kind: "decl"
    type: string
    name: string
    value: AST
}
export interface AAssign {
    kind: "assign"
    name: string
    op?: string
    value: AST
}
export interface AFunction {
    kind: "function"
    name: string
    arguments: { name: string, type: string }[]
    body: AST
}
export interface AReturn {
    kind: "return"
    value: AST
}
export interface ANumberLiteral {
    kind: "literal-number"
    value: number
}
export interface AStringLiteral {
    kind: "literal-string"
    value: string
}
export interface ABinaryOperation {
    kind: "binary-operation",
    a: AST,
    b: AST
    op: string
}
export interface AUnaryOperation {
    kind: "unary-operation"
    a: AST
    op: string
}
export interface AReference {
    kind: "ref",
    name: string
}
export interface ACall {
    kind: "call",
    fn: AST,
    arguments: AST[]
}
export interface AMember {
    kind: "member"
    name: string,
    base: AST
}
export interface AArrayLiteral {
    kind: "literal-array",
    elements: AST[]
}